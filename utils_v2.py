import os
from contextlib import contextmanager
import subprocess
import json

@contextmanager
def cd(newdir):
    '''
    funzione di cambio di directory
    '''
    prevdir = os.getcwd()
    os.chdir(os.path.expanduser(newdir))
    try:
        yield
    finally:
        os.chdir(prevdir)

class DatiClass:
    '''
    Classe di supporto per i parametri di configurazione dei dati reali e sintetici
    '''
    def __init__(self, width, height, disparity, extension, percent, path, right):
        self.width = width
        self.height = height
        self.disparity = disparity
        self.extension = extension
        self.percent = percent
        self.path = path
        self.right = right

def get_configuration_parameters():
    '''
    funzione di recupero della configurazione
    restituisce il dizionario contenente tutti i parametri
    '''
    cfg = dict()
    with open("configuration.json", "r") as f:
        cfg = json.load(f)
    classes = len(cfg["classes"])
    filters = (5 + classes) * 5
    cfg["filters"] = filters
    cfg["classnum"] = classes
    return cfg

def write(cfg, path):
    '''
    funzione di scrittura della configurazione
    scrive il dizionario in un file di configurazione json
    '''
    json_string = json.dumps(cfg, sort_keys=True, indent=4)
    with open(path, "w") as f:
        f.write(json_string)


def change_file(dest_path, input_path, patterns):
    """
    La funzione prende in ingresso dei pattern e controlla se il file li contiene
    Se una riga contiene il patter allora lo sostituisce con il valore corrispondente
    presente nella struttura dati patterns.
    Ad esempio, definendo **BATCH** come pattern, lo script cambiera' la riga
    batch =**BATCH** con batch = 64
    """
    if os.path.isfile(dest_path):
        os.remove(dest_path)
    with open(dest_path, 'w+') as temp:
        with open(input_path, "r") as f:
            for line in f:
                for chiave in patterns.keys():
                    if line.find(chiave) != -1:
                        line = line.replace(chiave, str(patterns[chiave]))
                temp.write(line)

def run(path, cmd):
    '''
    metodo di esecuzione di cambio di directory
    ed esecuzione del comando
    '''
    with cd(path):
        subprocess.call(cmd)

#main di prova necessario per il test della libreria
if __name__ == "__main__":

    faster_dict = dict()
    faster_dict["directory"] = "/home/lorenzo/Scrivania/faster-cnn"
    faster_dict["iou_th"] = 0.5

    yolo_dict = dict()
    yolo_dict["directory"] = "/home/lorenzo/Scrivania/darknet"
    yolo_dict["subdivisions"] = 8

    d = dict()
    d["cnn"] = "yolo"
    d["batch"] = 32
    d["classes"] = ["emptySpace", "runningLow"]
    d["training_perc"] = 0.1
    d["config_dir"] = "/home/lorenzo/Scrivania/script_dataset/config"
    d["test_dir"] = "/home/lorenzo/Scrivania/My_Dataset/test"
    dati_sintetici = DatiClass(640.0, 480.0, 30.0, ".png", 0.2, "/home/lorenzo/Scrivania/My_Dataset/Dataset", False)
    d["dati_sintetici"] = dati_sintetici.__dict__
    dati_reali = DatiClass(640.0, 480.0, 30.0, ".jpg", 1.0, "/home/lorenzo/Scrivania/My_Dataset/ZED_footage", False)
    d["dati_reali"] = dati_reali.__dict__
    d["new_dir_path"] = "/home/lorenzo/Scrivania/script_dataset/myDir"
    d["epoch"] = 30
    d["synt_labels_file_prefix"] = "bb_frame"

    d["faster_rcnn"] = faster_dict
    d["yolo"] = yolo_dict

    write(d, "configuration.json")

    configuration = get_configuration_parameters()

    #recupero oil nome della rete che si vuole utilizzare
    RETE = configuration["cnn"]

    #parola iniziale della stringa di interesse
    PATTERNS = configuration["classes"]

    #nomi dei file temporanei che verranno generati
    TEMP_LEFT_FILE_NAME = "temp_L.txt"
    TEMP_RIGHT_FILE_NAME = "temp_R.txt"

    #nome della directory da cui iniziare la ricerca dei file
    ROOT_REAL_DIR = configuration["dati_reali"]["path"]
    ROOT_SYNTH_DIR = configuration["dati_sintetici"]["path"]
    ROOT_CONFIG_DIR = configuration["config_dir"]
    ROOT_TEST_DIR = configuration["test_dir"]

    SYNTH_LABELS_FILE_PREFIX = configuration["synt_labels_file_prefix"]

    #percentuale di dati da utilizzare per il training
    TRAINING_PERC = configuration["training_perc"]

    #percentuale di immagini sintetiche e reali da utilizzare
    #le percentuali sono espresse sul numero totali di immagini della tipologia
    #ad esempio REAL_PERC = 0.7 significa che utilizzero il 70% delle
    #immagini reali che ho a disposizione
    REAL_PERC = configuration["dati_reali"]["percent"]
    SYNTH_PERC = configuration["dati_sintetici"]["percent"]
    REAL_EXT = configuration["dati_reali"]["extension"]

    #nome della directory contentente YOLO
    DARKNET_DIR = configuration["yolo"]["directory"]

    #nome della directori contenente faster cnn
    RCNN_DIR = configuration["faster_rcnn"]["directory"]
    #valore di soglia utilizzato
    RCNN_IOU_TH = configuration["faster_rcnn"]["iou_th"]

    #nome dei file di training e validazione che verranno creati
    TRAIN_FILE_NAME = "train.txt"
    VALID_FILE_NAME = "validation.txt"

    #parametri utilizzati per il calcolo del centro nelle immagini di destra
    #contengono la disparita espressa in pixel e la larghezza delle immagini
    #espressa in pixel
    REAL_DISPARITY = configuration["dati_reali"]["disparity"]
    REAL_IMAGE_WIDTH = configuration["dati_reali"]["width"]
    SYNTH_DISPARITY = configuration["dati_sintetici"]["disparity"]
    SYNTH_IMAGE_WIDTH = configuration["dati_sintetici"]["width"]

    FILTERS = configuration["filters"]

    EPOCH = configuration["epoch"]
