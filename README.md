# Attività progettuale Computer Vision
Il seguente lavoro è stato svolto per semplificare l'addestramento di Reti Neurali Convoluzionali sfruttando dataset con immagini reali e sintetiche realizzate dal [Computer Vision Lab](http://www.vision.disi.unibo.it)  

## Contenuto del repository
La repository contiene:
 - **script..:** script python3 in grado di riorganizzare un dataset composto da immagini e annotazioni in modo che sia possibile utilizzarlo per allenare [Yolo](https://pjreddie.com/darknet/yolo/) oppure
[Faster-RCNN](https://papers.nips.cc/paper/5638-faster-r-cnn-towards-real-time-object-detection-with-region-proposal-networks). Lo script è in grado anche di avviare il training della rete selezionata in configurazione.
 - **configuration.json:** il file di configurazione che permette di impostare in maniera semplice e veloce i parametri della rete
 - **utils_v2.py**: file python3 contenente alcune utilities.
 - **config:** cartella contenente alcuni file di configurazione utilizzati come Template e necessari per andare a settare i parametri delle reti. In particolare:
    - **obj.data:** utilizzato per la configurazione di Yolo. Viene aggiornato dallo script per generare il vero *obj.data* utilizzato da Yolo.  
    - **yolo-obj.cfg:** template del file di configurazione della rete yolo. Viene aggiornato dallo script per generare il vero *yolo-obj.cfg* utilizzato nelle fasi successive.  
    - **faster:** cartella contente il file template *my_dataset.yml* nel quale sono presenti i valori di configurazione di Faster.  
 - **detector.c:** versione modificata del file originario *detector.c* presente in *darknet/examples/dataset*. 

## Parametri del file di configurazione
Il file *configuration.json* contiene le seguenti voci:  
- **batch:** Questo valore assume due significati differenti. Per Yolo rappresenta il numero di immagini che vengono analizzate prima di aggiornare i pesi, mentre per Faster rappresenta il numero
- di blocchi in cui l'immagine viene divisa.
- **classes:** nome delle classi da utilizzare durante la fase di training, validazione e test della rete. I nomi delle classi devono corrispondere ai nomi utilizzati all'interno dei file di annotazione.
- **cnn:** tipo di rete utilizzata. I valori ammissibili in questo campo sono *yolo* oppure *faster_rcnn*.
- **config_dir:** percorso alla cartella di configurazione (che contiene i file template *obj.data* e *yolo-obj.cfg* e la cartella *faster*).
- **new\_dir\_path:** percorso alla cartella che conterrà i link simbolici alle immagini e i nuovi file di labels (per motivi di gestione dello spazio nella nuova cartella non verranno copiate le immagini del dataset ma bensì verranno creati dei link simbolici).
- **test\_dir:** percorso alla cartella che contiene le immagini ed i relativi dati che verranno utilizzate per la fase di testing finale. (Questa cartella dovrà avere una struttura interna analoga a quella delle immagini reali che verrà descritta successivamente).
- **yolo:** parametri di configurazione di Yolo. Questo oggetto contiene alcuni campi configurabili:
    - **directory:** percorso alla directory Darknet.
    - **subdivisions:** numero di threads che devono elaborare un batch.
- **faster\_rcnn:** parametri di configurazione di Faster. Questo oggetto contiene alcuni campi configurabili:
    - **directory:** percorso alla directory di Faster.
    - **iou\_th:** soglia di Intersection over Union utilizzata in fase di test.
- **training\_perc:** percentuale del dataset che verrà destinata alla fase di training (la restante parte verrà utilizzata per la fase di validazione).  
Nel caso di 0.9, ad esempio, di tutto il dataset il 90% delle immagini sarà usato per il training e solo il 10% per la validazione.
- **epoch:** numero di epoche che dovrà essere utilizzato per il train della rete.
- **synt\_labels\_file\_prefix:** prefisso del nome del file di annotazione da considerare per le immagini sintetiche (questo nome verrà poi sostituito dalla parola *frame* nel nome del file di annotazione di destinazione, è presente un esempio nelle sezioni successive del documento).
- **dati reali** e **dati sintetici** permettono invece di configurare i parametri di gestione delle immagini reali e sintetiche che costituiscono il dataset.  
    Per entrambi bisogna fornire:  
    - **right:** indica se devono essere utilizzate anche le immagini di destra (utile se è stata utilizzata una camera stereo).
    - **extension:** cioè l'estensione dei file immagine.
    - **percent:** percentuale dei dati da considerare. Se percent vale 0.5 ad esempio solo metà del dataset verrà considerato per il train e la validazione della rete. 
    - **height:** altezza espressa in pixel delle immagini del dataset (ad esempio 480).
    - **width:**larghezza espressa in pixel delle immagini del dataset (ad esempio 640).
    - **disparity:** nel caso in cui il flag di *right* sia a true, questo parametro indica la disparità della camera utilizzata.
    - **path:** percorso al dataset.

## Organizzazione del Dataset
Per lavorare correttamente lo script si aspetta che il dataset sia organizzato secondo una struttura ben definita. Questa struttura varia a seconda del tipo di dati utilizzato (reali/sintetici)

### Dati reali 

 ```
    |-- path  
        |-- ...    
        |-- Cartella_con_nome_qualsiasi_1  
            |...
            |-- left      
                |-- 0.jpg  
                |-- 1.jpg  
                |-- ...     
            |-- right
                |-- 0.jpg  
                |-- 1.jpg 
                |-- ...   
            |-- labels
                |-- 0.txt  
                |-- 1.txt  
                |-- ...  
        |-- Cartella_con_nome_qualsiasi_2
        |-- ...    
```

Lo script analizza tutte le cartelle presenti nella cartella dati reali indicata in configurazione (*path* nell'esempio). Tutte le cartelle al suo interno possono avere nomi arbitari, ma dentro devono contenere almeno le cartelle di *left* e *labels* (eventualmente quella di *right* se il flag corrispondente è a true).
Le eventuali altre cartelle presenti vengono ignorate.
Vengono creati link simbolici alle immagini della cartella *right* solo se il flag *right* in configurazione è a *true*.
Nella cartella labels devono esserci le labels relative alle immagini di left, in quanto quelle di right vengono calcolate automaticamente sfruttando la disparità (se richiesto). 

### Dati sintetici

Se il flag di right è posto a true la cartella deve essere configurata come segue:  

```
    |-- path  
        |-- ...    
        |-- Cartella_con_nome_qualsiasi_1  
            |-- ...
            |-- Frames      
                |-- 0000_L.jpg  
                |-- 0000_R.jpg
                |-- 0001_L.jpg  
                |-- 0001_R.jpg   
                |-- ...     
            |-- Bounding boxes
                |-- bb_0000.txt
                |-- bb_0001.txt
                |-- ...  
        |-- Cartella_con_nome_qualsiasi_2
        |-- ...    

``` 

I file possono chiamarsi come vogliono, basta che il nome scelto per indicare un certo frame (es 0000) sia lo stesso anche nella label. Per distinguere il frame destro dal sinistro devono essere utilizzati i suffissi _R ed _L.
Il nome dei file di labels deve iniziare con il suffisso specificato in fase di configurazione e può continuare come vuole (es bb\_frame\_0000.txt)  
**NB:** SI TENGA PRESENTE CHE IL SUFFISSO VIENE AUTOMATICAMENTE SOSTITUITO CON LA PAROLA *frame* E CHE **YOLO** SI ASPETTA CHE IL NOME DELLA IMMAGINE E DEL CORRISPETTIVO FILE DI LABEL DIFFERISCA PER LA SOLA ESTENSIONE, SE AD ESEMPIO LE IMMAGINI SONO NEL FORMATO *frame\_0000.jpg* E LE LABELS NEL FORMATO *bb\_shape\_0000.txt* PREFIX DOVRA VALERE *bb\_shape*

Se invece non si vogliono considerare le immagini di destra (flag right a false) viene meno il vincolo sul nome del file (terminazione con _L e _R). 

### Annotazioni dataset sintetico

Lo script si aspetta annotazioni dei dati sintetici nel seguente formato:

```txt
emptySpace.001  0.7426129281520844   0.3031410872936249  0.16116338968276978  0.2256872057914734
```

i cui campi sono, a partire dal primo:
 - **nome della classe:** lo script è configurato per eliminare la parte numerica successiva al punto. Questo nome verrà sostituito, nel file di annotazione generato dallo script, con un valore numerico.
 - **coordinata x del centro:**  valore normalizzato della coordinata x del centro della bounding box.
 - **coordinata y del centro:** valore normalizzato della coordinata y del centro della bounding box.
 - **width:** valore normalizzato della lunghezza della bounding box.
 - **height:** valore normalizzato dell'altezza della bounding box.

Affinché l'annotazione riportata precedentemente sia considerata significativa per il train, deve essere presente nella voce *classes* del file di configurazione
anche *empySpace*, altrimenti lo script scarta la riga.

### Annotazioni dataset reale e di test

Lo script si aspetta annotazioni per i dati reali e di test nel seguente formato:

```txt
0  0.7426129281520844   0.3031410872936249  0.16116338968276978  0.2256872057914734
```

i cui campi sono, a partire dal primo:
 - **indice della classe:** valore intero. Questo valore è l'indice della classe nella lista delle classi nel file di configurazione.
 - **coordinata x del centro:**  valore normalizzato della coordinata x del centro della bounding box.
 - **coordinata y del centro:** valore normalizzato della coordinata y del centro della bounding box.
 - **width:** valore normalizzato della lunghezza della bounding box.
 - **height:** valore normalizzato dell'altezza della bounding box.

Eventualmente, lo script è in grado di gestire anche la situazione in cui le annotazioni hanno un campo aggiuntivo:

```txt
0  0.7426129281520844   0.3031410872936249  0.16116338968276978  0.2256872057914734 0
```

i cui campi sono, a partire dal primo:
 - **indice della classe:** valore intero. Questo valore è l'indice della classe nella lista delle classi nel file di configurazione.
 - **coordinata x del centro:**  valore normalizzato della coordinata x del centro della bounding box.
 - **coordinata y del centro:** valore normalizzato della coordinata y del centro della bounding box.
 - **width:** valore normalizzato della lunghezza della bounding box.
 - **height:** valore normalizzato dell'altezza della bounding box.
 - **indicatore:** valore intero 

Lo script è programmato per eliminare l'ultimo valore numerico (l'indicatore) in quanto non è necessario. 

> Prestare attenzione a configurare correttamente altezza e larghezza delle immagini reali e sintetiche nel file **configuration.json**, in quanto verranno utilizzate per calcolare valori assoluti a partire da quelli relativi espressi nelle annotazioni. 


> NB: Quando vengono calcolate le annotazioni per le bounding box delle immagini di destra vengono scartati tutti i box che non sono contenuti nell'immagine

# Configurazione Yolo 

## Prima configurazione
La prima cosa da fare è seguire la procedura di installazione di Yolo presente nel sito e riportata per completezza.

1. Aprire un terminale e digitare

    ```sh
    git clone https://github.com/pjreddie/darknet
    ```

2. Spostarsi nella cartella di Darknet

    ```sh
    cd darknet
    ```

3. Compilare Yolo tramite Make.

    ```sh
    make
    ```

    *NB: Se si vuole compilare Yolo per usare la GPU oppure OpenCV occorre cambiare i flag all'interno del Makefile prima di lanciare il comando make*

Una volta compilato correttamente Yolo è possibile procedere con la fase di configurazione attraverso lo script da noi creato.  Le fasi da seguire sono le seguenti:

1. Aprire un nuovo terminale, posizionarsi in una cartella e scaricare il progetto dal repository con il comando clone di git:

    ```sh
    git clone https://gitlab.com/filippoaleotti/attivita-progettuale-cnn
    ```

2. Cambiare la configurazione della rete nel file *configuration.json*. 
   In particolare controllare che cnn abbia valore *yolo*

3. Scaricare il [file dei pesi](https://pjreddie.com/media/files/darknet19_448.conv.23) e copiarlo dentro la directory Darknet

4. Lanciare lo script  

    ```sh
    python3 cfg_yolo_v5.py 
    ```
    
    Lo script realizzato andrà a popolare la directory specificata nel file di configurazione a partire dalle cartelle che costituiscono il dataset. La directory di output conterrà tre cartelle separate per i dati di training, validazione e test. Lo script si occuperà di generare i file txt necessari per le tre fasi citate precedentemente ed avvierà il training della rete che durerà un numero 
    di epoche pari al valore definito in configurazione.

5. Nella cartella di Backup (indicata in *obj.data*) verrà creato un file *yolo-obj_final.weights* al termine del training.

## Successive configurazioni
Durante la fase di configurazione la rete trasferisce tutti i dati del dataset all'interno della directory di destinazione e crea dei file *elaborated.txt* all'interno delle directory di input già visitate per evitare di dover esguire elaborazioni inutili nei lanci successivi.  
Quando si esegue nuovamente lo script ci si può trovare di fronte a due diverse situazioni che verranno descritte successivamente.

### Dataset invariati
Si vuole riaddestrare la rete modificando semplicemente una delle percentuali utilizzate precedentemente.

In questo caso è sufficiente modificare il solo file di configurazione *configuration.json* e lanciare lo script python come al punto 4.  
Lo script infatti si occuperà di generare dei nuovi file txt e di avviare un nuovo training senza eseguire inutili elaborazioni.

### Modifica del dataset
Se si decide di modifcare il valore del flag right risulta necessario eliminare dalle cartelle di input i file *elaborated.txt* in modo da permettere allo script di rigenerare i dati necessari per il training.  
In questo caso risulta necessario eliminare la cartella *new_dir_path* e modificare il file *configuration.json*.

## Eseguire validazione e test
La fase di validazione della rete può essere eseguita aprendo un terminale all'interno della cartella *darknet* e digitando il seguente comando:

```sh
./darknet detector valid ./cfg/obj.data ./cfg/yolo-obj.cfg ./yolo-obj_final.weights
```

dove il file *yolo-obj_final.weights* è il file generato nella fase di training precedente e può eventualmente essere sostituito con un file di backup del training in esecuzione.  
Questo operazione andrà a generare un file per ogni classe all'interno della cartella *result* presente in *darknet*.  
Ognuno di quei file avrà un contenuto simile al seguente.

``` txt
1151_L 0.020862 305.952728 456.455200 337.733187 480.000000
1151_L 0.022543 353.736816 463.590637 384.813004 480.000000
1151_L 0.009536 298.286194 496.256134 347.814886 480.000000
1151_L 0.009359 345.867859 501.864197 396.091787 480.000000
```
In cui ogni riga contiene tutte le informazioni relative ad una singola bounding box di una immagine di *validation.txt*.

Il metodo *yolo_detector_recall* è stato invece modificato per ottenere informazioni più significative durante la fase di validation e testing.
Per poter utilizzare il nuovo metodo è necessario sostituire il file */examples/detector.c* di Darknet con il file *detector_v2.c* presente nel repository, aprire un terminale nella directory Darknet e lanciare

``` sh
make clean
make
```

Il comando per avviarlo è il seguente:

```sh
./darknet detector recall ./cfg/yolo-obj.cfg ./yolo-obj_final.weights ./test.txt ./recall_result
```

dove */test.txt* rappresenta il file contenente la lista delle immagini di test(o evntualmente di validazione) mentre *./recall_result* è il path alla cartella di output che conterrà il risultato.

Il file di output avrà il seguente contenuto

``` txt
Risultati del classificatore dipendenti dalle classi
IOU_th;TP;FP;Recall;Precision
0.00%;11052; 9432;66.22%;53.95%;
0.05%;11025; 9459;66.05%;53.82%;
0.10%;10985; 9499;65.81%;53.63%;
0.15%;10919; 9565;65.42%;53.31%;
0.20%;10808; 9676;64.75%;52.76%;
0.25%;10686; 9798;64.02%;52.17%;
0.30%;10522; 9962;63.04%;51.37%;
0.35%;10286;10198;61.63%;50.21%;
0.40%; 9950;10534;59.61%;48.57%;
0.45%; 9431;11053;56.50%;46.04%;
0.50%; 8793;11691;52.68%;42.93%;
----------------------------------------------------------------------------
Risultati del classificatore indipendenti dalle classi
IOU_th;TP;FP;Recall;Precision
0.00%;11857; 8627;71.04%;57.88%;
0.05%;11842; 8642;70.95%;57.81%;
0.10%;11828; 8656;70.86%;57.74%;
0.15%;11791; 8693;70.64%;57.56%;
0.20%;11720; 8764;70.22%;57.22%;
0.25%;11607; 8877;69.54%;56.66%;
0.30%;11418; 9066;68.41%;55.74%;
0.35%;11158; 9326;66.85%;54.47%;
0.40%;10792; 9692;64.66%;52.69%;
0.45%;10262;10222;61.48%;50.10%;
0.50%; 9613;10871;57.59%;46.93%;
```

Ogni riga di ogni tabella contiene i valori di recall, precision, true positive e false positive per ogni diverse soglie di iou_th.
Vengono generate due tabelle in quanto la prima effettua il controllo sulla classe mentre la seconda no (considera giuste anche le predizioni che superano la soglia di threeshold ma che hanno appartengono alla classe sbagliata).

È stata inoltre creata un'ulteriore versione del file detector.c che permette di effettuare delle validazioni aggiuntive.  
Il file *detector_v3* mostra come variano i risultati ottenuti in funzione del valore di soglia di confidenza utilizzato per filtrare le detection.  
Il file generato attraverso *detector_v3* è il seguente, come si può notare i valori nella prima colonna a sinistra sono ora riferiti alla soglia di confidenza mentre il valore di IOU_th è pari a 0.5. 

``` txt
Risultati del classificatore dipendenti dalle classi
CONF_th;TP;FP;Recall;Precision
0.00%;11449;5140808;68.61%;0.22%;
0.10%; 9338;20149;55.96%;31.67%;
0.20%; 8962;13631;53.70%;39.67%;
0.30%; 8656;10345;51.87%;45.56%;
0.40%; 8330; 8039;49.92%;50.89%;
0.50%; 7878; 5924;47.21%;57.08%;
0.60%; 6865; 3893;41.14%;63.81%;
0.70%; 4781; 1848;28.65%;72.12%;
0.80%; 1440;  240;8.63%;85.71%;
----------------------------------------------------------------------------
Risultati del classificatore indipendenti dalle classi
CONF_th;TP;FP;Recall;Precision
0.00%;12983;5139274;77.80%;0.25%;
0.10%;10371;19116;62.15%;35.17%;
0.20%; 9812;12781;58.80%;43.43%;
0.30%; 9400; 9601;56.33%;49.47%;
0.40%; 8947; 7422;53.61%;54.66%;
0.50%; 8384; 5418;50.24%;60.74%;
0.60%; 7228; 3530;43.31%;67.19%;
0.70%; 4920; 1709;29.48%;74.22%;
0.80%; 1459;  221;8.74%;86.85%;
```

# Configurazione Faster-RCNN
La versione di Faster utilizzata è implementata in Tensorflow ed si trova nel seguente [repository](https://gitlab.com/filippoaleotti/faster-cnn).

## Prima configurazione
Per poter allenare la rete occorre:

1. Aprire un terminale e digitare

    ```sh
    git clone https://gitlab.com/filippoaleotti/faster-cnn
    ```
    
2. Build dei Cython modules

    ```Shell
    cd TFFRCNN/lib
    make # compile cython and roi_pooling_op, you may need to modify make.sh for your platform
    ```

3. Cambiare oppurtunamente il file di configurazione *configuration.json*

4. Lanciare lo script  

    ```sh
    python3 cfg_yolo_v5.py 
    ```

    Lo script realizzato andrà a popolare la directory specificata nel file di configurazione a partire dalle cartelle che costituiscono il dataset. La directory di output conterrà tre cartelle separate per i dati di training, validazione e test. Lo script si occuperà di generare i file txt necessari per le tre fasi citate precedentemente.
    Verrà generato il file di configurazione */experiments/cfgs/my_dataset.yml* per Faster (a partire dai valori inseriti nel file *configuration.json*).  
    Successivamente verrà avviato il training della rete che durerà un numero di epoche pari al valore definito in configurazione e, una volta terminato, verrà avviato anche il test.  
    **NB**: il file dei pesi con cui effettuare il test viene recuperato automaticamente da uno script interno a Faster

> Se non si utilizza Faster su macchina con GPU Titan X occorre controllare i parametri nello script python, ad esempio il tipo di architettura

## Successive configurazioni
Durante la fase di configurazione la rete trasferisce tutti i dati del dataset all'interno della directory di destinazione e crea dei file *elaborated.txt* all'interno delle directory di input già visitate per evitare di dover esguire elaborazioni inutili nei lanci successivi.  
Quando si esegue nuovamente lo script ci si può trovare di fronte a due diverse situazioni che verranno descritte successivamente.

### Dataset invariati
Si vuole riaddestrare la rete modificando semplicemente una delle percentuali utilizzate precedentemente.  
In questo caso è sufficiente modificare il solo file di configurazione *configuration.json* e lanciare lo script python come al punto 4.  
Lo script infatti si occuperà di generare dei nuovi file txt e di avviare un nuovo training senza eseguire inutili elaborazioni.

### Modifica del dataset
Se si decide di modifcare il valore del flag right risulta necessario eliminare dalle cartelle di input i file *elaborated.txt* in modo da permettere allo script di rigenerare i dati necessari per il training.  
In questo caso risulta necessario eliminare la cartella *new\_dir\_path* e modificare il file *configuration.json*.  

### Output
Quando il test è terminato, viene prodotto nella cartella */output* un file il cui nome è la concatenazione della percentuale dei dati reali utilizzati, quella dei dati sintetici e N0.
Se si effettua il train+test di un dataset in cui all'interno del file *configuration.json* era specificato *dati\_reali[percent] = 0.5* e *dati\_sintetici[percent] = 0.2* allora il file si chiamerà
*R05\_S02\_N0*.
Il file conterrà una tabella in cui, per ogni classe, viene indicato il valore di precision e recall medio.
La riga *AVG* contiene la media dei valori delle singole classi (che rappresenta il risultato complessivo della rete), mentre la riga *CLI* (*class indipendent*) contiene valori di precision e recall
calcolati non considerando l'equivalenza tra classe predetta e ground truth (come detto in Yolo).  
Tutti i valori precedentemente descritti sono ottenuti utilizzando una soglia di confidenza pari a 0 in fase di filtraggio delle detection.
Le righe sotto ogni classe permettono di vedere la variazione dei valori di recall e precision in funzione della soglia di confidenza utilizzata per il filtraggio delle detection (I valori nella prima colonna rappresentano il valore di soglia utilizzato).  
 
``` txt
+Risultato test------+-------+-------+
| Results    | AP    | PREC  | REC   |
+------------+-------+-------+-------+
| emptySpace | 0.455 | 0.423 | 0.576 |
| 0.0        | ---   | 0.423 | 0.576 |
| 0.1        | ---   | 0.462 | 0.569 |
| 0.2        | ---   | 0.499 | 0.559 |
| 0.3        | ---   | 0.519 | 0.551 |
| 0.4        | ---   | 0.535 | 0.544 |
| 0.5        | ---   | 0.551 | 0.537 |
| 0.6        | ---   | 0.564 | 0.529 |
| 0.7        | ---   | 0.578 | 0.517 |
| runningLow | 0.177 | 0.157 | 0.567 |
| 0.0        | ---   | 0.157 | 0.567 |
| 0.1        | ---   | 0.177 | 0.550 |
| 0.2        | ---   | 0.195 | 0.526 |
| 0.3        | ---   | 0.207 | 0.514 |
| 0.4        | ---   | 0.219 | 0.508 |
| 0.5        | ---   | 0.228 | 0.497 |
| 0.6        | ---   | 0.237 | 0.489 |
| 0.7        | ---   | 0.246 | 0.482 |
| AVG        | 0.316 | 0.290 | 0.572 |
+------------+-------+-------+-------+
| CLI        | 0.425 | 0.365 | 0.615 |
+------------+-------+-------+-------+
```
    
> Per visualizzare le bounding boxes dell'immagine puoi usare il [visualizzatore di bounding boxes](https://gitlab.com/filippoaleotti/visualizzatore-bb)