"""
@06/05/2017
@Authors Lorenzo Ciotti, Filippo Aleotti
@Attivita' progettuale Computer Vision

SCRIPT PER IMPOSTARE UN DATASET DI IMMAGINI E LABELS
IN MODO CHE YOLO POSSA UTILIZZATO PER IL TRAINING
"""

import os
import sys
import argparse
import random
import shutil
from utils_v2 import *
from tqdm import tqdm
from PIL import Image
from distutils.dir_util import copy_tree
import yaml
 
#recupero i parametri di configurazione dal file configuration.json
configuration = get_configuration_parameters()

#recupero il nome della rete che si vuole utilizzare
RETE = configuration["cnn"]

#recupero i valori di classes dal file di configurazione
PATTERNS = configuration["classes"]

#nomi dei file temporanei che verranno generati
TEMP_LEFT_FILE_NAME = "temp_L.txt"
TEMP_RIGHT_FILE_NAME = "temp_R.txt"

#nomi delle directory necessarie allo script
ROOT_REAL_DIR = configuration["dati_reali"]["path"]
ROOT_SYNTH_DIR = configuration["dati_sintetici"]["path"]
ROOT_CONFIG_DIR = configuration["config_dir"]
ROOT_TEST_DIR = configuration["test_dir"]

#prefisso dei file di labels di interesse per i dati sintetici
SYNTH_LABELS_FILE_PREFIX = configuration["synt_labels_file_prefix"]

#percentuale di dati da utilizzare per il training
TRAINING_PERC = configuration["training_perc"]

#percentuale di immagini sintetiche e reali da utilizzare
#le percentuali sono espresse sul numero totali di immagini della tipologia
#ad esempio REAL_PERC = 0.7 significa che utilizzero il 70% delle
#immagini reali che ho a disposizione
REAL_PERC = configuration["dati_reali"]["percent"]
SYNTH_PERC = configuration["dati_sintetici"]["percent"]

#estensioni delle immagini dei due dataset
REAL_EXT = configuration["dati_reali"]["extension"]
SYNTH_EXT = configuration["dati_sintetici"]["extension"]

#nome della directory contentente YOLO
DARKNET_DIR = configuration["yolo"]["directory"]

#nome della directori contenente faster rcnn
RCNN_DIR = configuration["faster_rcnn"]["directory"]

#valore di soglia utilizzato per il test in faster rcnn
RCNN_IOU_TH = configuration["faster_rcnn"]["iou_th"]

#nome dei file di training e validazione che verranno creati
TRAIN_FILE_NAME = "train.txt"
VALID_FILE_NAME = "validation.txt"

#parametri utilizzati per il calcolo del centro nelle immagini di destra
#contengono la disparita espressa in pixel e la larghezza delle immagini
#espressa in pixel
REAL_RIGHT = configuration["dati_reali"]["right"]
REAL_DISPARITY = configuration["dati_reali"]["disparity"]
REAL_IMAGE_WIDTH = configuration["dati_reali"]["width"]

SYNTH_RIGHT = configuration["dati_sintetici"]["right"]
SYNTH_DISPARITY = configuration["dati_sintetici"]["disparity"]
SYNTH_IMAGE_WIDTH = configuration["dati_sintetici"]["width"]

#recuper il valore di filtri necessari per configurare yolo
FILTERS = configuration["filters"]

#recupero il path alla directory di destinazione
#del dataset elaborato
NEW_DIR_PATH = configuration["new_dir_path"]

#recupero il numero di epoche di durata del training
EPOCH = configuration["epoch"]

#nome del file utilizzato per capire se la cartella e' gia' stata elaborata
FLAG_FILE_NAME = "elaborated.txt"

#converto in assoluti tutti i path di interesse
ROOT_SYNTH_DIR = os.path.abspath(ROOT_SYNTH_DIR)
ROOT_REAL_DIR = os.path.abspath(ROOT_REAL_DIR)
ROOT_CONFIG_DIR = os.path.abspath(ROOT_CONFIG_DIR)
ROOT_TEST_DIR = os.path.abspath(ROOT_TEST_DIR)
NEW_DIR_PATH = os.path.abspath(NEW_DIR_PATH)
DARKNET_DIR = os.path.abspath(DARKNET_DIR)
RCNN_DIR = os.path.abspath(RCNN_DIR)

#controllo che le directory inserite nel file di configurazione esistano
if not os.path.exists(ROOT_SYNTH_DIR):
    print("la directory %s non esiste" %ROOT_SYNTH_DIR)
    exit()
if not os.path.exists(ROOT_REAL_DIR):
    print("la directory %s non esiste" %ROOT_REAL_DIR)
    exit()
if not os.path.exists(ROOT_CONFIG_DIR):
    print("la directory %s non esiste" %ROOT_CONFIG_DIR)
    exit()
if not os.path.exists(ROOT_TEST_DIR):
    print("la directory %s non esiste" %ROOT_TEST_DIR)
    exit()
if not os.path.exists(DARKNET_DIR) and RETE == "yolo":
    print("la directory %s non esiste" %DARKNET_DIR)
    exit()
if not os.path.exists(RCNN_DIR) and RETE == "faster_rcnn":
    print("la directory %s non esiste" %RCNN_DIR)
    exit()

print("Directory contenente i dati sintetici: %s" %ROOT_SYNTH_DIR)
print("Directory contenente i dati reali: %s" %ROOT_REAL_DIR)
print("Directory contenente i file per il test: %s" %ROOT_TEST_DIR)
print("Directory contenente i file di configurazione: %s" %ROOT_CONFIG_DIR)
print("Directory che conterra i file per il training: %s" %NEW_DIR_PATH)
print("Directory Darknet: %s" %DARKNET_DIR)
print("")

# ****************             ELABORAZIONE DEI DATI SINTETICI                 ****************
# **************** RIORGANIZZAZIONE DELLE DIRECTORIES ED EVENTUALE CONVERSIONE ****************

print("INIZIO LA FASE DI ELABORAZIONE DEI DATI SINTETICI")

#elimino eventuali file temporanei precedenti
if os.path.isfile(TEMP_LEFT_FILE_NAME):
    os.remove(TEMP_LEFT_FILE_NAME)
if os.path.isfile(TEMP_RIGHT_FILE_NAME):
    os.remove(TEMP_RIGHT_FILE_NAME)

#creo il path alla nuova directory che conterrà i dati sintetici
synth_root_path = os.path.join(NEW_DIR_PATH, "synth")

#creo il path ai file di flag
flag_file_path = os.path.join(ROOT_SYNTH_DIR, FLAG_FILE_NAME)
#se il file di flag non esiste lo creo
#ed inizio l'elaborazione dei dati
if not os.path.isfile(flag_file_path):
    with open(flag_file_path, 'w') as temp:
        temp.write("questo file serve solo per ricordarmi che ho elaborato questa directory")
        #se la cartella in cui devo salvare i file di training esiste mi blocco
        if os.path.exists(synth_root_path):
            print("la directory {} esiste gia".format(synth_root_path))
            print("inserire una nuova directory o eliminare quella esistente")
            sys.exit()
        else:
            os.makedirs(synth_root_path)

    print("Inizio la fase di copia dei file nella nuova directory")
    #recupero il numero di elementi, necessario per le barre di caricamento
    filecounter = 0
    for root, dirs, files in os.walk(ROOT_SYNTH_DIR):
        filecounter += 1

    #inizio l'elaborazione dei file sintetici
    for root, dirs, files in tqdm(os.walk(ROOT_SYNTH_DIR), total=filecounter):
        #creo nella nuova cartella le directory "images" e "labels"
        #che verranno utilizzate da yolo durante il training
        for d_name in dirs:
            absolute_d_name = os.path.abspath(os.path.join(root, d_name))
            temp_dir_name = absolute_d_name.replace(ROOT_SYNTH_DIR, synth_root_path)
            if d_name == "Frames":
                #creo la cartella in cui poi andro ad inserire i link alle immagini
                temp_dir_name = temp_dir_name.replace("Frames", "images")
                os.makedirs(temp_dir_name)
            if d_name == "Bounding boxes":
                #copio le cartelle contenenti le labels con il loro intero contenuto
                temp_dir_name = temp_dir_name.replace("Bounding boxes", "labels")
                shutil.copytree(absolute_d_name, temp_dir_name)

        for f_name in files:
            #creo un link alle immagini presenti nella cartella "Frame" all'interno
            #della cartella "images" precedentemente creata
            absolute_f_name = os.path.abspath(os.path.join(root, f_name))
            if root.endswith("Frames") and absolute_f_name.endswith(SYNTH_EXT):
                absolute_link_name = absolute_f_name.replace(ROOT_SYNTH_DIR, synth_root_path)
                absolute_link_name = absolute_link_name.replace("Frames", "images")
                if not SYNTH_RIGHT:
                    if absolute_f_name.endswith("_R%s" %SYNTH_EXT):
                        continue
                    elif not absolute_f_name.endswith("_L%s" %SYNTH_EXT):
                        absolute_link_name = absolute_link_name.replace("%s" %SYNTH_EXT, "_L%s" %SYNTH_EXT)
                os.symlink(absolute_f_name, absolute_link_name)


    print("")
    print("Inizio la fase di elaborazione dei file nella nuova directory")
    filecounter = 0
    #recupero il numero di elementi, necessario per le barre di caricamento
    for root, dirs, files in os.walk(synth_root_path):
        filecounter += 1

    counter = 0
    for root, dirs, files in tqdm(os.walk(synth_root_path), total=filecounter):
        #rimuovo tutte le annotazioni che non contengono i pattern di interesse
        #e converto opportunamente i pattern
        #infine copio i file modificati nelle nuove directory create precedentemente
        for f_name in files:
            #mi permette di controllare se il bounding box della immagine di destra e' ancora
            #all'interno dell'immagine
            right_ok = True
            foundLabel = False

            absolute_f_name = os.path.abspath(os.path.join(root, f_name))
            #se ho di fronte un file di testo contenuto nella cartella associata ai bounding box
            if absolute_f_name.endswith(".txt") and absolute_f_name.find("labels") != -1:
                #genero il path dei file che conterranno le annotazioni
                #delle immagini di destra e di sinistra a partire dal path del file attuale
                if not absolute_f_name.find(SYNTH_LABELS_FILE_PREFIX) != -1:
                    os.remove(absolute_f_name)
                    continue
                counter += 1
                #base_name = f_name.split("_" , 1)[1]
                base_name = f_name.replace(SYNTH_LABELS_FILE_PREFIX, "frame")
                base_name = base_name.split('.', 1)[0]
                f_left_name = "%s_L.txt" %base_name
                f_right_name = "%s_R.txt" %base_name
                absolute_f_left_name = os.path.join(root, f_left_name)
                absolute_f_right_name = os.path.join(root, f_right_name)

                #apro il file di annotazione in lettura e creo i file temporanei in scrittura
                #i file temporanei verranno poi opportunamente rinominati
                with open(absolute_f_name, 'r') as f:
                    with open(TEMP_LEFT_FILE_NAME, 'w') as f_left_out:
                        with open(TEMP_RIGHT_FILE_NAME, 'w') as f_right_out:
                            #ottengo tutte le linee presenti nel file
                            lines = f.readlines()
                            for l in lines:
                                #ottengo le singole parole che formano la linea del file
                                #la funzione sotto serve per rimuovere gli spazi duplicati
                                #presenti nei file
                                l = " ".join(l.split())
                                words = l.split(' ', 2)
                                first_word = words[0]
                                #se nella prima parola e' presente un . vado ad eliminare
                                #il numero successivo ad esso
                                if "." in first_word:
                                    first_word = first_word.split(".")[0]

                                #vado a vericare se la prima parola della stringa
                                #contiene uno dei pattern tra quelli cercati
                                if any(x in first_word for x in PATTERNS):
                                    matches = [x for x in PATTERNS if x in first_word]
                                    first_word = matches[0]
                                    #recupero la cordinata x del centro per effettuare la correzione
                                    #e scrivo la riga corrispondente nel file di destra se il centro
                                    #non si trova fuori l'immagine
                                    pattern_index = repr(PATTERNS.index(first_word))
                                    center_x = words[1]
                                    fixed_center_x = float(center_x) - SYNTH_DISPARITY/SYNTH_IMAGE_WIDTH
                                    sequence_r = (pattern_index, repr(fixed_center_x), words[2])
                                    right_line = " ".join(sequence_r)
                                    if fixed_center_x >= 0:
                                        f_right_out.write(right_line+"\n")
                                    #scrivo la riga corrispondente al file di sinistra
                                    sequence_l = (pattern_index, words[1], words[2])
                                    left_line = " ".join(sequence_l)
                                    f_left_out.write(left_line+"\n")

                #elimino il file originale
                os.remove(absolute_f_name)

                #rinomino i file temporanei nei file di destinazione
                os.rename(TEMP_LEFT_FILE_NAME, absolute_f_left_name)
                if SYNTH_RIGHT:
                    os.rename(TEMP_RIGHT_FILE_NAME, absolute_f_right_name)

    print("file elaborati: %i" %counter)
    print("")


# ****************             ELABORAZIONE DEI DATI REALI                     ****************
# **************** RIORGANIZZAZIONE DELLE DIRECTORIES ED EVENTUALE CONVERSIONE ****************
#elimino eventuali file temporanei precedenti
if os.path.isfile(TEMP_LEFT_FILE_NAME):
    os.remove(TEMP_LEFT_FILE_NAME)
if os.path.isfile(TEMP_RIGHT_FILE_NAME):
    os.remove(TEMP_RIGHT_FILE_NAME)

print("")
print("")
print("INIZIO LA FASE DI ELABORAZIONE DEI DATI REALI")

#creo il path alla nuova cartella che conterrà i dati reali
real_root_path = os.path.join(NEW_DIR_PATH, "real")

#creo il path al file di flag
flag_file_path = os.path.join(ROOT_REAL_DIR, FLAG_FILE_NAME)

#se il file di flag non esiste inizio la fase di elaborazione
#dei dati reali
if not os.path.isfile(flag_file_path):
    with open(flag_file_path, 'w') as temp:
        temp.write("questo file serve solo per ricordarmi che ho elaborato questa directory")

        if os.path.exists(real_root_path):
            print("la directory {} esiste gia".format(real_root_path))
            print("inserire una nuova directory o eliminare quella esistente")
            sys.exit()
        else:
            os.makedirs(real_root_path)

    print("Inizio la fase di copia dei file nella nuova directory")
    filecounter = 0
    #recupero il numero di elementi, necessario per le barre di caricamento
    for root, dirs, files in os.walk(ROOT_REAL_DIR):
        filecounter += 1

    #inizio l'elaborazione dei dati
    for root, dirs, files in tqdm(os.walk(ROOT_REAL_DIR), total=filecounter):
        for d_name in dirs:
            #se trovo una cartella left creo la cartella images nella nuova
            #cartella del dataset
            #copio la cartella labels
            absolute_d_name = os.path.abspath(os.path.join(root, d_name))
            temp_dir_name = absolute_d_name.replace(ROOT_REAL_DIR, real_root_path)
            if d_name == "left":
                image_dir_name = temp_dir_name.replace("left", "images")
                os.makedirs(image_dir_name)
            if d_name == "labels":
                shutil.copytree(absolute_d_name, temp_dir_name)

        for f_name in files:
            #creo un link alle immagini presenti nella cartelle "left" e "right" all'interno
            #della cartella "images" precedentemente creata
            #aggiungo i suffissi "L.txt" e "R.txt" ai link creati
            absolute_f_name = os.path.abspath(os.path.join(root, f_name))

            if absolute_f_name.find("left") != -1 and absolute_f_name.endswith(REAL_EXT):
                absolute_link_name = absolute_f_name.replace(ROOT_REAL_DIR, real_root_path)
                absolute_link_name = absolute_link_name.replace("left", "images")
                absolute_link_name = absolute_link_name.replace(REAL_EXT, "_L%s" %REAL_EXT)
                os.symlink(absolute_f_name, absolute_link_name)

            if absolute_f_name.find("right") != -1 and absolute_f_name.endswith(REAL_EXT) and REAL_RIGHT:
                absolute_link_name = absolute_f_name.replace(ROOT_REAL_DIR, real_root_path)
                absolute_link_name = absolute_link_name.replace("right", "images")
                absolute_link_name = absolute_link_name.replace(REAL_EXT, "_R%s" %REAL_EXT)
                os.symlink(absolute_f_name, absolute_link_name)


    print("")
    print("Inizio la fase di elaborazione dei file nella nuova directory")
    filecounter = 0
    #recupero il numero di elementi, necessario per le barre di caricamento
    for root, dirs, files in os.walk(real_root_path):
        filecounter += 1

    #sistemo i file di annotazione delle immagini di sinistra
    #vado a verificare se per ogni link alle immagini di sinistra
    #esiste un file di annotazione, se esiste elimino il campo che non serve (lo 0 finale)
    #altrimenti creo un file vuoto
    print("Fase 1/2")
    for root, dirs, files in tqdm(os.walk(real_root_path, topdown=False), total=filecounter):
        for file_name in files:
            absolute_name = os.path.abspath(os.path.join(root, file_name))

            if absolute_name.find("images") != -1 and absolute_name.endswith("_L%s" %REAL_EXT):
                absolute_f_name = absolute_name.replace("images", "labels")
                absolute_f_name = absolute_f_name.replace("_L%s" %REAL_EXT, ".txt")
                if not os.path.isfile(absolute_f_name):
                    absolute_f_left_name = absolute_f_name.replace(".txt", "_L.txt")
                    open(absolute_f_left_name, "w").close()
                else:
                    with open(absolute_f_name, 'r') as f:
                        with open(TEMP_LEFT_FILE_NAME, 'w') as f_left_out:
                            lines = f.readlines()
                            for l in lines:
                                #ottengo le singole parole che formano la linea del file
                                #la funzione sotto serve per rimuovere gli spazi duplicati
                                #presenti nei file
                                left_line = " ".join(l.split())
                                #rimuovo il quinto valore dalla linea che ho letto
                                #solo se esiste
                                if len(left_line.split()) == 6:
                                    left_line = left_line[::-1]
                                    left_line = left_line.split(" ", 1)[1]
                                    left_line = left_line[::-1]

                                #scrivo la riga nel file di destra
                                f_left_out.write(left_line+"\n")

                    os.remove(absolute_f_name)
                    absolute_f_left_name = absolute_f_name.replace(".txt", "_L.txt")
                    os.rename(TEMP_LEFT_FILE_NAME, absolute_f_left_name)

    #vado a sistemare le annotazioni relative alle immagini di destra
    #opero esattamente come nel caso precedente
    #l'unica differenza e' che ora lavoro con i file txt prodotti sopra
    #quindi manca il quinto compo e ho gia il suffisso "_L" al nome
    if REAL_RIGHT:
        filecounter = 0
        for root, dirs, files in os.walk(real_root_path):
            filecounter += 1

        print("Fase 2/2")
        for root, dirs, files in tqdm(os.walk(real_root_path, topdown=False), total=filecounter):
            for file_name in files:
                absolute_name = os.path.abspath(os.path.join(root, file_name))
                if absolute_name.find("images") != -1 and absolute_name.endswith("_R%s" %REAL_EXT):
                    right_ok = True
                    absolute_f_name = absolute_name.replace("images", "labels")
                    absolute_f_name = absolute_f_name.replace("_R%s" %REAL_EXT, "_L.txt")
                    if not os.path.isfile(absolute_f_name):
                        absolute_f_right_name = absolute_f_name.replace("_L.txt", "_R.txt")
                        open(absolute_f_right_name, "w").close()
                    else:
                        with open(absolute_f_name, 'r') as f:
                            with open(TEMP_RIGHT_FILE_NAME, 'w') as f_right_out:
                                lines = f.readlines()
                                for l in lines:
                                    #ottengo le singole parole che formano la linea del file
                                    #la funzione sotto serve per rimuovere gli spazi duplicati
                                    #presenti nei file
                                    l = " ".join(l.split())
                                    words = l.split(' ', 2)

                                    #recupero la cordinata x del centro per effettuare la correzione
                                    center_x = words[1]
                                    fixed_center_x = float(center_x) - REAL_DISPARITY/REAL_IMAGE_WIDTH
                                    #creo la nuova riga che andra memorizzata nel file di destra
                                    sequence_r = (words[0], repr(fixed_center_x), words[2])
                                    right_line = " ".join(sequence_r)
                                    #scrivo la riga nel file di destra se il centro si trova
                                    #all'interno dell'immagine
                                    if fixed_center_x >= 0:
                                        f_right_out.write(right_line+"\n")

                        absolute_f_right_name = absolute_f_name.replace("_L.txt", "_R.txt")
                        os.rename(TEMP_RIGHT_FILE_NAME, absolute_f_right_name)

# ****************             ELABORAZIONE DEI DATI DI TEST                   ****************
#elimino eventuali file temporanei precedenti
if os.path.isfile(TEMP_LEFT_FILE_NAME):
    os.remove(TEMP_LEFT_FILE_NAME)
if os.path.isfile(TEMP_RIGHT_FILE_NAME):
    os.remove(TEMP_RIGHT_FILE_NAME)

#i dati di test sono formati da sole immagini reali
print("")
print("")
print("INIZIO LA FASE DI ELABORAZIONE DEI DATI DI TEST")

#creo il path alla nuova cartella che conterrà i dati di test
test_root_path = os.path.join(NEW_DIR_PATH, "test")

#creo il path al file di flag
flag_file_path = os.path.join(ROOT_TEST_DIR, FLAG_FILE_NAME)

#se il file di flag non esiste inizio l'elaborazione dei dati di test
if not os.path.isfile(flag_file_path):
    with open(flag_file_path, 'w') as temp:
        temp.write("questo file serve solo per ricordarmi che ho elaborato questa directory")
        #se la cartella in cui devo salvare i file di training esiste mi blocco
        if os.path.exists(test_root_path):
            print("la directory {} esiste gia".format(test_root_path))
            print("inserire una nuova directory o eliminare quella esistente")
            sys.exit()
        else:
            os.makedirs(test_root_path)

    print("Inizio la fase di copia dei file nella nuova directory")
    filecounter = 0
    #recupero il numero di elementi, necessario per le barre di caricamento
    for root, dirs, files in os.walk(ROOT_TEST_DIR):
        filecounter += 1

    #inizio l'elaborazione dei dati di test
    for root, dirs, files in tqdm(os.walk(ROOT_TEST_DIR), total=filecounter):
        #per ogni cartella di left creo una cartella images
        #nella nuova cartella di test del dataset
        #copio le cartelle contenenti le annotazioni
        for d_name in dirs:
            absolute_d_name = os.path.abspath(os.path.join(root, d_name))
            temp_dir_name = absolute_d_name.replace(ROOT_TEST_DIR, test_root_path)
            if d_name == "left":
                image_dir_name = temp_dir_name.replace("left", "images")
                os.makedirs(image_dir_name)
            if d_name == "labels":
                shutil.copytree(absolute_d_name, temp_dir_name)

        for f_name in files:
            #creo un link alle immagini presenti nella cartelle "left" e "right" all'interno
            #della cartella "images" precedentemente creata
            #aggiungo i suffissi "L.txt" e "R.txt" ai link creati
            absolute_f_name = os.path.abspath(os.path.join(root, f_name))

            if absolute_f_name.find("left") != -1 and absolute_f_name.endswith(REAL_EXT):
                absolute_link_name = absolute_f_name.replace(ROOT_TEST_DIR, test_root_path)
                absolute_link_name = absolute_link_name.replace("left", "images")
                absolute_link_name = absolute_link_name.replace(REAL_EXT, "_L%s" %REAL_EXT)
                os.symlink(absolute_f_name, absolute_link_name)

            if absolute_f_name.find("right") != -1 and absolute_f_name.endswith(REAL_EXT) and REAL_RIGHT:
                absolute_link_name = absolute_f_name.replace(ROOT_TEST_DIR, test_root_path)
                absolute_link_name = absolute_link_name.replace("right", "images")
                absolute_link_name = absolute_link_name.replace(REAL_EXT, "_R%s" %REAL_EXT)
                os.symlink(absolute_f_name, absolute_link_name)


    print("")
    print("Inizio la fase di elaborazione dei file nella nuova directory")
    filecounter = 0
    #recupero il numero di elementi, necessario per le barre di caricamento
    for root, dirs, files in os.walk(test_root_path):
        filecounter += 1

    #sistemo i file di annotazione delle immagini di sinistra
    #vado a verificare se per ogni link alle immagini di sinistra
    #esiste un file di annotazione, se esiste elimino il campo che non serve (lo 0 finale)
    #altrimenti creo un file vuoto
    print("Fase 1/2")
    for root, dirs, files in tqdm(os.walk(test_root_path, topdown=False), total=filecounter):
        for file_name in files:
            absolute_name = os.path.abspath(os.path.join(root, file_name))

            if absolute_name.find("images") != -1 and absolute_name.endswith("_L%s" %REAL_EXT):
                absolute_f_name = absolute_name.replace("images", "labels")
                absolute_f_name = absolute_f_name.replace("_L%s" %REAL_EXT, ".txt")
                if not os.path.isfile(absolute_f_name):
                    absolute_f_left_name = absolute_f_name.replace(".txt", "_L.txt")
                    open(absolute_f_left_name, "w").close()
                else:
                    with open(absolute_f_name, 'r') as f:
                        with open(TEMP_LEFT_FILE_NAME, 'w') as f_left_out:
                            lines = f.readlines()
                            for l in lines:
                                #ottengo le singole parole che formano la linea del file
                                #la funzione sotto serve per rimuovere gli spazi duplicati
                                #presenti nei file
                                left_line = " ".join(l.split())
                                #rimuovo il quinto valore dalla linea che ho letto
                                #solo se esiste
                                if len(left_line.split()) == 6:
                                    left_line = left_line[::-1]
                                    left_line = left_line.split(" ", 1)[1]
                                    left_line = left_line[::-1]

                                #scrivo la riga nel file di destra
                                f_left_out.write(left_line+"\n")

                    os.remove(absolute_f_name)
                    absolute_f_left_name = absolute_f_name.replace(".txt", "_L.txt")
                    os.rename(TEMP_LEFT_FILE_NAME, absolute_f_left_name)

    #vado a sistemarele le annotazioni relative alle immagini di destra
    #opero esattamente come nel caso precedente
    #l'unica differenza e' che ora lavoro con i file txt prodotti sopra
    #quindi manca il quinto compo e ho gia il suffisso "_L" al nome
    if REAL_RIGHT:
        filecounter = 0
        for root, dirs, files in os.walk(test_root_path):
            filecounter += 1

        print("Fase 2/2")
        for root, dirs, files in tqdm(os.walk(test_root_path, topdown=False), total=filecounter):
            for file_name in files:
                absolute_name = os.path.abspath(os.path.join(root, file_name))
                if absolute_name.find("images") != -1 and absolute_name.endswith("_R%s" %REAL_EXT):
                    right_ok = True
                    absolute_f_name = absolute_name.replace("images", "labels")
                    absolute_f_name = absolute_f_name.replace("_R%s" %REAL_EXT, "_L.txt")
                    if not os.path.isfile(absolute_f_name):
                        absolute_f_right_name = absolute_f_name.replace("_L.txt", "_R.txt")
                        open(absolute_f_right_name, "w").close()
                    else:
                        with open(absolute_f_name, 'r') as f:
                            with open(TEMP_RIGHT_FILE_NAME, 'w') as f_right_out:
                                lines = f.readlines()
                                for l in lines:
                                    #ottengo le singole parole che formano la linea del file
                                    #la funzione sotto serve per rimuovere gli spazi duplicati
                                    #presenti nei file
                                    l = " ".join(l.split())
                                    words = l.split(' ', 2)

                                    #recupero la cordinata x del centro per effettuare la correzione
                                    center_x = words[1]
                                    fixed_center_x = float(center_x) - REAL_DISPARITY/REAL_IMAGE_WIDTH
                                    #creo la nuova riga che andra memorizzata nel file di destra
                                    sequence_r = (words[0], repr(fixed_center_x), words[2])
                                    right_line = " ".join(sequence_r)
                                    #scrivo la riga nel file di destra se il centro si trova
                                    #all'interno dell'immagine
                                    if fixed_center_x >= 0:
                                        f_right_out.write(right_line+"\n")

                        absolute_f_right_name = absolute_f_name.replace("_L.txt", "_R.txt")
                        os.rename(TEMP_RIGHT_FILE_NAME, absolute_f_right_name)

# **************** CREAZIONE DEI FILE TRAIN.TXT ,VALID.TXT E TEST.TXT ****************
print("")
print("")

#elimino eventuali file di training e validazione precedenti
TRAIN_FILE = os.path.join(ROOT_CONFIG_DIR, "train.txt")
VALIDATION_FILE = os.path.join(ROOT_CONFIG_DIR, "valid.txt")
TEST_FILE = os.path.join(ROOT_CONFIG_DIR, "test.txt")

if os.path.isfile(TRAIN_FILE):
    os.remove(TRAIN_FILE)

if os.path.isfile(VALIDATION_FILE):
    os.remove(VALIDATION_FILE)

if os.path.isfile(TEST_FILE):
    os.remove(TEST_FILE)

print("Avvio la fase di creazione dei file train.txt, valid.txt e test.txt")

#numero di immagini utilizzate per la fase di training
training_images_number = 0

#creo i nuovi file di training e validazione
out_file = open(TRAIN_FILE, "w")
out_file_valid = open(VALIDATION_FILE, "w")
out_file_test = open(TEST_FILE, "w")

filecounter = 0
#recupero il numero di elementi, necessario per le barre di caricamento
for root, dirs, files in os.walk(test_root_path):
    filecounter += 1

print("fase 1/3")

#per ogni file nell'albero che ha come radice la cartella test_root_path
for root, dirs, files in tqdm(os.walk(test_root_path), total=filecounter):
    for f_name in files:
        absolute_f_name = os.path.abspath(os.path.join(root, f_name))
        #se il file ha estensione SYNTH_EXT o REAL_EXT
        #aggiungo il nome del file alla fine della lista
        if absolute_f_name.endswith(SYNTH_EXT) or absolute_f_name.endswith(REAL_EXT):
            out_file_test.write("%s\n" %absolute_f_name)

out_file_test.close()

list_image = []

filecounter = 0
#recupero il numero di elementi, necessario per le barre di caricamento
for root, dirs, files in os.walk(synth_root_path):
    filecounter += 1

print("fase 2/3")

#per ogni file nell'albero che ha come radice la cartella synth_root_path
for root, dirs, files in tqdm(os.walk(synth_root_path), total=filecounter):
    for f_name in files:
        randNumber = random.randint(0, 99)
        absolute_f_name = os.path.abspath(os.path.join(root, f_name))
        #se il file ha estensione SYNTH_EXT e si trova nella directory images
        #aggiungo il nome del file alla fine della lista
        #l'aggiunta del file dipende anche dalla probabilità legata alla
        #percentuale di dati da utilizzare espressa nel file di configurazione
        if absolute_f_name.endswith(SYNTH_EXT):
            if absolute_f_name.find("images") != -1:
                if randNumber < SYNTH_PERC * 100:
                    list_image.append(absolute_f_name)

filecounter = 0
#recupero il numero di elementi, necessario per le barre di caricamento
for root, dirs, files in os.walk(real_root_path):
    filecounter += 1

print("fase 3/3")

#per ogni file nell'albero che ha come radice la cartella real_root_path
for root, dirs, files in tqdm(os.walk(real_root_path), total=filecounter):
    for f_name in files:
        randNumber = random.randint(0, 99)
        absolute_f_name = os.path.abspath(os.path.join(root, f_name))
        #se il file ha estensione REAL_EXT e si trova nella directory images
        #aggiungo il nome del file alla fine della lista
        #l'aggiunta del file dipende anche dalla probabilità legata alla
        #percentuale di dati da utilizzare espressa nel file di configurazione
        if absolute_f_name.endswith(REAL_EXT):
            if absolute_f_name.find("images") != -1:
                if randNumber < REAL_PERC * 100:
                    list_image.append(absolute_f_name)

#ordino la lista in modo casuale
random.shuffle(list_image)

#per ogni nome nella lista aggiungo il nome in uno dei due file
#in base ad un valore casuale estratto
for f_name in list_image:
    randNumber = random.randint(0, 99)
    if randNumber < TRAINING_PERC * 100:
        out_file.write(f_name + "\n")
        training_images_number += 1
    else:
        out_file_valid.write(f_name + "\n")

#chiudo i due file di destinatione
out_file.close()
out_file_valid.close()

print("Creazione dei file di training e validation terminata")
print("")

#configurazione di yolo
if RETE == "yolo":

    #se  nella configurazione è stato scelto di utilizzare yolo allora
    #modifico i file di configurazione di yolo
    #e avvio il training
    print("aggiungo i file di configurazione yolo alla directory YOLO")
    #aggiungo i file di configurazione di yolo
    cfg_dir = "%s/cfg" %(DARKNET_DIR)
    if not os.path.exists(cfg_dir):
        sys.exit("Errore: non e' stata trovata la cartella cfg all'interno della cartella "+DARKNET_DIR+".\nControllare che il percorso a Darknet sia corretto")

    #personalizzo il file di configurazione di yolo yolo-obj.cfg
    patterns = {}
    patterns["**FILTERS**"] = configuration["filters"]
    patterns["**CLASSNUM**"] = configuration["classnum"]
    patterns["**BATCH**"] = configuration["batch"]
    patterns["**SUBDIVISIONS**"] = configuration["yolo"]["subdivisions"]
    max_bacthes = int((EPOCH * training_images_number) / configuration["batch"])
    patterns["**MAX_BATCHES**"] = max_bacthes

    yolo_cfg_file_input_cfg = os.path.abspath(ROOT_CONFIG_DIR)+"/yolo-obj.cfg"
    yolo_cfg_file_input_data = os.path.abspath(ROOT_CONFIG_DIR)+"/obj.data"

    yolo_cfg_file_output_cfg = os.path.abspath(DARKNET_DIR)+"/cfg/yolo-obj.cfg"
    yolo_cfg_file_output_data = os.path.abspath(DARKNET_DIR)+"/cfg/obj.data"

    change_file(yolo_cfg_file_output_cfg, yolo_cfg_file_input_cfg, patterns)

    patterns = {}
    yolo_cfg_file_output_data = os.path.abspath(DARKNET_DIR)+"/cfg/obj.data"
    patterns["**CLASSNUM**"] = configuration["classnum"]
    patterns["**TRAIN**"] = os.path.abspath(TRAIN_FILE)
    patterns["**VALID**"] = os.path.abspath(VALIDATION_FILE)

    change_file(yolo_cfg_file_output_data, yolo_cfg_file_input_data, patterns)

    #copia di names
    data_dir = "%s/data" %(DARKNET_DIR)
    yolo_names = os.path.abspath(DARKNET_DIR)+"/data/obj.names"
    if os.path.isfile(yolo_names):
        os.remove(yolo_names)
    with open(yolo_names,"w+") as names:
        for c in configuration["classes"]:
            names.write("%s\n" %c)

    print("")
    print("lancio il training di yolo =)")
    print("")

    #lancio il train della rete
    cmd = ["./darknet", "detector", "train", "cfg/obj.data", "cfg/yolo-obj.cfg", "darknet19_448.conv.23"]
    run(DARKNET_DIR, cmd)

#configurazione di faster rcnn
if RETE == "faster_rcnn":
    #se  nella configurazione è stato scelto di utilizzare faster cnn allora
    #modifico i file di configurazione di faster cnn
    #e avvio il training
    print ("aggiungo i file di configurazione faster cnn alla directory FASTER CNN")

    #controllo che la cartella di configurazione per il file json di faster esista
    json_rcnn_cfg_dir = "%s/configuration" %(RCNN_DIR)
    if not os.path.exists(json_rcnn_cfg_dir):
        sys.exit("Errore: non e' stata trovata la cartella configuration all'interno della cartella "+RCNN_DIR+".\nControllare che il percorso sia corretto")

    #controllo che la cartella di configurazione per il file yml di faster esista
    yml_rcnn_cfg_dir = "%s/experiments/cfgs" %(RCNN_DIR)
    if not os.path.exists(yml_rcnn_cfg_dir):
        sys.exit("Errore: non e' stata trovata la cartella experiments/cfgs all'interno della cartella "+RCNN_DIR+".\nControllare che il percorso sia corretto")

    configuration["test_image_txt"] = os.path.abspath(TEST_FILE)
    configuration["train_image_txt"] = os.path.abspath(TRAIN_FILE)
    configuration["valid_image_txt"] = os.path.abspath(VALIDATION_FILE)

    write(configuration, os.path.join(json_rcnn_cfg_dir, "configuration.json"))

    #vado a leggere il file YAML nella mia directory interna
    #lo modifico e poi lo trasferisco nella directory di faster
    with open("./config/faster/my_dataset.yml", 'r') as stream:
        try:
            yml_config = yaml.load(stream)
        except yaml.YAMLError as exc:
            print(exc)

    yml_config["TRAIN"]["BATCH_SIZE"] = configuration["batch"]
    yml_config["NCLASSES"] = configuration["classnum"] + 1

    with open(os.path.join(yml_rcnn_cfg_dir, "my_dataset.yml"), 'w') as outfile:
        yaml.dump(yml_config, outfile, default_flow_style=True)

    #se presente elimino le cartelle di cache di faaster
    #perchè sto utilizzando delle diverse immagini per il training
    cache_dir_path = os.path.join(RCNN_DIR, "data", "cache")
    if os.path.isdir(cache_dir_path):
        shutil.rmtree(cache_dir_path)

    cache_dir_path = os.path.join(RCNN_DIR, "temp_file", "annotations_cache")
    if os.path.isdir(cache_dir_path):
        shutil.rmtree(cache_dir_path)

    cache_dir_path = os.path.join(RCNN_DIR, "data", "cache")
    if os.path.isdir(cache_dir_path):
        shutil.rmtree(cache_dir_path)

    #elimino i file di log relativi ai precedenti train
    log_dir_path = os.path.join(RCNN_DIR, "logs")
    for root, dirs, files in os.walk(log_dir_path):
        for file in files:
            file = os.path.join(root, file)
            os.remove(file)

    print("")
    print("lancio il training di faster cnn =)")
    print("")

    #lancio il train della rete
    cmd = ["./experiments/scripts/faster_rcnn_end2end.sh", "0", "VGG16", "my_dataset"]
    run(RCNN_DIR, cmd)