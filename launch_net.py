import json
import os
from contextlib import contextmanager
import subprocess
import shutil
from time import gmtime, strftime

@contextmanager
def cd(newdir):
    prevdir = os.getcwd()
    os.chdir(os.path.expanduser(newdir))
    try:
        yield
    finally:
        os.chdir(prevdir)

#lista in cui ogni elemento è una coppia
#[PERC_REAL, PERC_SYNT]
PERCENT_LIST = [[1.0,0.0], [0.5, 0.0], [0.5, 0.2], [0.5, 0.5], [0.0, 1.0]]

#cartella in cui verranno copiati i risultati
BACKUP_DIR="/media/Data/AleottiCiotti/Backup_yolo"

#path del file di configurazione json dello script
CONFIG_FILE_PATH="/home/tesista/Desktop/AleottiCiotti/attivita-progettuale-cnn/configuration.json"

#path alla directory in cui si trova lo script di training
SCRIPT_PATH = "/home/tesista/Desktop/AleottiCiotti/attivita-progettuale-cnn"

#path alla cartella contenente i pesi
WEIGHT_DIR_PATH = "/media/Data/AleottiCiotti/Backup"

#path alla cartella contenente il file di risultato del test
RESULT_DIR_PATH = "/home/tesista/Desktop/AleottiCiotti/attivita-progettuale-cnn/recall_result"

#path alla cartella contenente i file di test, train e valid
TXT_DIR_PATH = "/home/tesista/Desktop/AleottiCiotti/attivita-progettuale-cnn/config"

#comando da lanciare per eseguire il train
train_cmd = ["python3", "cfg_yolo_v5.py"]

#path alle directory che potrebbero contenere elaborated.txt
DATASET_DIRS = ["/media/Data/Dataset/AlmaMarket/new",
                "/media/Data/Dataset/SupermarketReal/Test_set",
                "/media/Data/Dataset/SupermarketReal/Training_Set"]

#elimino i file txt se sono presenti
for path in DATASET_DIRS:
    elaborated_file_path = os.path.join(path, "elaborated.txt")
    if os.path.isfile(elaborated_file_path):
        os.remove(elaborated_file_path)

cfg = dict()

for train in PERCENT_LIST:
    
    start_time = strftime("%Y-%m-%d %H:%M:%S", gmtime())

    REAL_PERC = train[0]
    SYNTH_PERC = train[1]

    REAL_STRING = str(REAL_PERC).replace(".", "")
    SYNTH_STRING = str(SYNTH_PERC).replace(".", "")

    with open(CONFIG_FILE_PATH, "r") as cf:
        cfg = json.load(cf)
    
    cfg["dati_reali"]["percent"] = REAL_PERC
    cfg["dati_sintetici"]["percent"] = SYNTH_PERC
    cfg["cnn"] = "yolo"

    with open(CONFIG_FILE_PATH, "w") as cf:
        json_string = json.dumps(cfg, sort_keys=False, indent=4)
        cf.write(json_string)
    
    with cd(SCRIPT_PATH):
        subprocess.call(train_cmd)

    print("training R{}_S{}_N0 terminato".format(REAL_STRING, SYNTH_STRING))

    dir_output_name = "R{}_S{}_N0".format(REAL_STRING, SYNTH_STRING)
    dir_output_path = os.path.join(BACKUP_DIR, dir_output_name)
    os.mkdir(dir_output_path)

    print("copia dei risultati in {}".format(dir_output_path))

    #sposto i file di pesi ottenuti dal train
    for root, dirs, files in os.walk(WEIGHT_DIR_PATH):
        for file_name in files:
            absolute_s_file_name = os.path.join(root, file_name)
            absolute_d_file_name = os.path.join(dir_output_path, file_name)
            shutil.move(absolute_s_file_name, absolute_d_file_name)

    #creo il comando per avviare il test
    weight_path = os.path.join(dir_output_path, "yolo-obj_final.weights")
    test_file_path = os.path.join(TXT_DIR_PATH, "test.txt")
    test_cmd = ["./darknet", "detector", "recall", weight_path, test_file_path, RESULT_DIR_PATH]
    with cd(SCRIPT_PATH):
        subprocess.call(test_cmd)

    #sposto i file di train, test e validation utilizzati
    train_s_file_path = os.path.join(TXT_DIR_PATH, "train.txt")
    test_s_file_path = os.path.join(TXT_DIR_PATH, "test.txt")
    valid_s_file_path = os.path.join(TXT_DIR_PATH, "valid.txt")
    train_d_file_path = os.path.join(dir_output_path, "train.txt")
    test_d_file_path = os.path.join(dir_output_path, "test.txt")
    valid_d_file_path = os.path.join(dir_output_path, "valid.txt")
    shutil.move(train_s_file_path, train_d_file_path)
    shutil.move(test_s_file_path, test_d_file_path)
    shutil.move(valid_s_file_path, valid_d_file_path)

    #sposto il file di risultato ottenuto dal test
    file_name = "R{}_S{}_N0.txt".format(REAL_STRING, SYNTH_STRING)
    absolute_s_file_name = os.path.join(RESULT_DIR_PATH, "result.txt")
    absolute_d_file_name = os.path.join(dir_output_path, file_name)
    shutil.move(absolute_s_file_name, absolute_d_file_name)


    end_time = strftime("%Y-%m-%d %H:%M:%S", gmtime())
    print("trasferimento terminato")

    log_file_path = os.path.join(dir_output_path, "log.txt")
    with open(log_file_path, "w") as lf:
        lf.write("percentuale reali: {}".format(REAL_PERC))
        lf.write("percentuale sintetici: {}".format(SYNTH_PERC))
        lf.write("ora inizio: {}".format(start_time))
        lf.write("ora fine: {}".format(end_time))
        